FROM composer:1.8.4
LABEL MAINTAINER="Victor Anuebunwa <hello@victoranuebunwa.com>"
COPY . /build
WORKDIR /build
RUN apk add --no-cache --virtual zip git && docker-php-ext-install -j$(nproc) pdo
RUN docker-php-ext-install pdo_mysql
RUN composer install --dry-run && php cli make:env
